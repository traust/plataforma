# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190517175051) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "zipcode"
    t.string "address"
    t.string "number"
    t.string "complement"
    t.string "neighborhood"
    t.string "city"
    t.string "state"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "backers", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "user_id"
    t.bigint "reward_id"
    t.float "value"
    t.string "status"
    t.string "payment_method"
    t.string "payment_id"
    t.string "payment_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "anonymous", default: false
    t.boolean "refund_if_not_success", default: false
    t.string "observation"
    t.string "street"
    t.string "zip_code"
    t.string "neighborhood"
    t.string "street_number"
    t.string "state"
    t.string "complement"
    t.string "city"
    t.index ["project_id"], name: "index_backers_on_project_id"
    t.index ["reward_id"], name: "index_backers_on_reward_id"
    t.index ["user_id"], name: "index_backers_on_user_id"
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "bank_code", default: ""
    t.string "agency", default: ""
    t.string "agency_vd", default: ""
    t.string "account", default: ""
    t.string "account_vd", default: ""
    t.string "account_type", default: "conta_corrente"
    t.string "legal_name", default: ""
    t.string "document_number", default: ""
    t.string "recipient_id"
    t.bigint "user_id"
    t.index ["recipient_id"], name: "index_bank_accounts_on_recipient_id"
    t.index ["user_id"], name: "index_bank_accounts_on_user_id"
  end

  create_table "conversations", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "recipient_id"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_conversations_on_project_id"
    t.index ["recipient_id"], name: "index_conversations_on_recipient_id"
    t.index ["sender_id"], name: "index_conversations_on_sender_id"
  end

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.boolean "is_read", default: false
    t.bigint "conversation_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "post_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "body"
    t.string "image_url"
    t.bigint "post_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_category_id"], name: "index_posts_on_post_category_id"
  end

  create_table "project_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "project_updates", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.bigint "project_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_project_updates_on_project_id"
    t.index ["user_id"], name: "index_project_updates_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id"
    t.bigint "project_category_id"
    t.float "goal", default: 0.0
    t.date "expires_at"
    t.text "about"
    t.text "headline"
    t.text "video_url"
    t.text "image_url"
    t.string "slug"
    t.string "status"
    t.boolean "visible", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "highlight", default: false
    t.text "budget_image_url"
    t.bigint "bank_account_id"
    t.index ["bank_account_id"], name: "index_projects_on_bank_account_id"
    t.index ["project_category_id"], name: "index_projects_on_project_category_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "rewards", force: :cascade do |t|
    t.bigint "project_id"
    t.float "minimum_value", default: 0.0
    t.integer "maximum_backers"
    t.text "description"
    t.text "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "delivery_date"
    t.string "observation_description"
    t.integer "position"
    t.index ["project_id"], name: "index_rewards_on_project_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.boolean "admin", default: false
    t.text "bio"
    t.string "avatar_url"
    t.string "provider"
    t.string "uid"
    t.string "token"
    t.integer "expires_at"
    t.boolean "expired"
    t.string "refresh_token"
    t.bigint "address_id"
    t.string "phone"
    t.string "phone_area_code"
    t.string "document"
    t.index ["address_id"], name: "index_users_on_address_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "backers", "projects"
  add_foreign_key "backers", "rewards"
  add_foreign_key "backers", "users"
  add_foreign_key "projects", "project_categories"
  add_foreign_key "projects", "users"
  add_foreign_key "rewards", "projects"

  create_view "project_totals",  sql_definition: <<-SQL
      SELECT backers.project_id,
      sum(backers.value) AS pledged,
      count(*) AS total_backers
     FROM backers
    WHERE ((backers.status)::text = 'approved'::text)
    GROUP BY backers.project_id;
  SQL

end
