class AddBudgetImageUrlToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :budget_image_url, :text
  end
end
