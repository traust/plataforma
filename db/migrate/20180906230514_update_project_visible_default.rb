class UpdateProjectVisibleDefault < ActiveRecord::Migration[5.1]
  def up
    change_column :projects, :visible, :boolean, default: false
  end

  def down
    change_column :projects, :visible, :boolean, default: true
  end
end
