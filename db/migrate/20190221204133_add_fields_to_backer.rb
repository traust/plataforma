class AddFieldsToBacker < ActiveRecord::Migration[5.1]
  def change
    add_column :backers, :street, :string
    add_column :backers, :zip_code, :string
    add_column :backers, :neighborhood, :string
    add_column :backers, :street_number, :string
    add_column :backers, :state, :string
    add_column :backers, :complement, :string
    add_column :backers, :city, :string
  end
end
