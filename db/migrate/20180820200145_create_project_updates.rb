class CreateProjectUpdates < ActiveRecord::Migration[5.1]
  def change
    create_table :project_updates do |t|
      t.string :title
      t.text :description
      t.references :project
      t.references :user

      t.timestamps
    end
  end
end
