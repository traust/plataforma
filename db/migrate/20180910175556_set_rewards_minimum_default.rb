class SetRewardsMinimumDefault < ActiveRecord::Migration[5.1]
  def up
    change_column :rewards, :minimum_value, :float, default: 0
  end

  def down
    change_column :rewards, :minimum_value, :float, default: nil
  end
end
