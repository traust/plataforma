class AddSubscriptionRequiredFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :phone, :string
    add_column :users, :phone_area_code, :string
    add_column :users, :document, :string
  end
end
