class AddHighlightToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :highlight, :boolean, default: false
  end
end
