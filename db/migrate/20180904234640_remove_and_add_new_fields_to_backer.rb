class RemoveAndAddNewFieldsToBacker < ActiveRecord::Migration[5.1]
  def change
    rename_column :backers, :payer_name, :payment_url
    remove_column :backers, :payer_email
    remove_column :backers, :payer_document
    remove_column :backers, :payment_token
  end
end
