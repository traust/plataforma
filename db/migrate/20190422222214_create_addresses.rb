class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :zipcode
      t.string :address
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :city
      t.string :state
      t.references :addressable, polymorphic: true, index: true

      t.timestamps
    end

    add_reference :users, :address, index: true
  end
end
