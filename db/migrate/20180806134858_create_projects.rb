class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.references :user, foreign_key: true
      t.references :project_category, foreign_key: true
      t.float :goal
      t.date :expires_at
      t.text :about
      t.text :headline
      t.text :video_url
      t.text :image_url
      t.string :slug
      t.string :status
      t.boolean :visible, default: true

      t.timestamps
    end
  end
end
