class CreateBackers < ActiveRecord::Migration[5.1]
  def change
    create_table :backers do |t|
      t.references :project, foreign_key: true
      t.references :user, foreign_key: true
      t.references :reward, foreign_key: true
      t.float :value
      t.string :status
      t.string :payment_method
      t.text :payment_token
      t.string :payment_id
      t.string :payer_name
      t.string :payer_email
      t.string :payer_document

      t.timestamps
    end
  end
end
