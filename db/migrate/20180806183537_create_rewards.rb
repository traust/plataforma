class CreateRewards < ActiveRecord::Migration[5.1]
  def change
    create_table :rewards do |t|
      t.references :project, foreign_key: true
      t.float :minimum_value
      t.integer :maximum_backers
      t.text :description
      t.text :image_url

      t.timestamps
    end
  end
end
