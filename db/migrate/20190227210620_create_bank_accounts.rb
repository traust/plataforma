class CreateBankAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :bank_accounts do |t|
      t.string :bank_code, default: ''
      t.string :agency, default: ''
      t.string :agency_vd, default: ''
      t.string :account, default: ''
      t.string :account_vd, default: ''
      t.string :account_type, default: 'conta_corrente'
      t.string :legal_name, default: ''
      t.string :document_number, default: ''
      t.string :recipient_id, index: true
      t.references :user, index: true

      t.timestamp
    end
  end
end
