class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :slug
      t.string :body
      t.string :image_url
      t.references :post_category, index: true

      t.timestamps
    end
  end
end
