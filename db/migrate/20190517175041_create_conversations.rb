class CreateConversations < ActiveRecord::Migration[5.1]
  def change
    create_table :conversations do |t|
      t.references :sender, index: true
      t.references :recipient, index: true
      t.references :project, index: true

      t.timestamps
    end
  end
end
