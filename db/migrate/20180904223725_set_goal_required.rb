class SetGoalRequired < ActiveRecord::Migration[5.1]
  def change
    change_column :projects, :goal, :float, :default => 0.0
  end
end
