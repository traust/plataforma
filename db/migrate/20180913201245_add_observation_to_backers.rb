class AddObservationToBackers < ActiveRecord::Migration[5.1]
  def change
    add_column :backers, :observation, :string
  end
end
