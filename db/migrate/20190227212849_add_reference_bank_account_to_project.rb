class AddReferenceBankAccountToProject < ActiveRecord::Migration[5.1]
  def change
    add_reference :projects, :bank_account, index: true
  end
end
