class AddAnonymousToBackers < ActiveRecord::Migration[5.1]
  def change
    add_column :backers, :anonymous, :boolean, default: false
    add_column :backers, :refund_if_not_success, :boolean, default: false
  end
end
