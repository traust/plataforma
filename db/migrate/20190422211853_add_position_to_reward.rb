class AddPositionToReward < ActiveRecord::Migration[5.1]
  def change
    add_column :rewards, :position, :integer
  end
end
