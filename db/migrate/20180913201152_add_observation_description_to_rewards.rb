class AddObservationDescriptionToRewards < ActiveRecord::Migration[5.1]
  def change
    add_column :rewards, :observation_description, :string
  end
end
