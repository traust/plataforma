SELECT
  backers.project_id AS project_id,
  SUM(backers.value) AS pledged,
  COUNT(*) AS total_backers
  FROM backers
  WHERE (backers.status = 'approved')
  GROUP BY backers.project_id
