class BackerMailerPreview < ActionMailer::Preview
  def receipt_mail
    BackerMailer.receipt_mail(Backer.first, 150.50)
  end
end
