class ProjectUpdateMailerPreview < ActionMailer::Preview
  def new_update_email
    ProjectUpdateMailer.new_update_email(ProjectUpdate.first)
  end
end
