# README

* Ruby version
ruby 2.6.5p114


* System dependencies

* Configuration
yarn install
gem install rails
sudo apt install ruby-full
sudo apt update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc

* Database creation
sudo -u postgres psql
sudo -u postgres createuser alster-dev
export ALSTER_DATABASE_PASSWORD="Pass@word1"
sudo -u postgres createdb alster_test
su - postgres

* Database initialization
bin/rails db:migrate RAILS_ENV=development


* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

* Database AWS
Identifier: AlsterProd
User: alsterDev
Pass: PaSsW0rD123