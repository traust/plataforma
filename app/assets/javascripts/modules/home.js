var Home = {
  setup: function () {
    mobileDevice = (window.innerWidth <= 526)
    options = {
      items: (mobileDevice) ? 1 : 2,
      loop: true,
      margin: 30,
      autoplay: true,
      dots: true
    }

    if (!mobileDevice) {
      options.dotsContainer = "#owl-dots-container"
    }

    $(".owl-carousel").owlCarousel(options);
  }
}

document.addEventListener("turbolinks:load", function () {
  Home.setup()
})
