var Projects = {
  setup: function() {
    $('.currency input').maskMoney({prefix:'R$ ', decimal:'.', affixesStay: false});
  },
  onUpload: function(event) {
    console.log(event);
    uploadFile = $(event.target).closest('.upload');
    uploadFile.text('Alterar imagem');
  }
}

$(document).ready(function() {
  Projects.setup();
});
