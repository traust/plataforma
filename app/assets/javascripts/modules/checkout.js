var Checkout = {
  checkoutForm: null,
  amount: null,
  setup: function() {
    $(document).on("click", ".payment-service-toggle", function(e) {
      Checkout.modal = $("#payment-service-modal")
      Checkout.modal.on("hide.bs.modal", function(e) {
        $("#paypal-button-container").empty()
      })
      Checkout.checkoutForm = $(this).closest(".checkout-form")
      Checkout.setFormAttributes()
      if (Checkout.verify() && Checkout.amount != 0) {
        try {
          Checkout.configurePaypalButton()
        } catch(e) {
          $("#paypal-button-container").append('<span class="alert alert-warning">Pagamento pelo Paypal indisponível no momento.</span>')
        }
        Checkout.modal.modal()
      }
    })

    $(document).on("click", "*[data-payment-service-button]", function(e) {
      Checkout.closeModal()
    })

    $(document).on("submit", ".checkout-form", function(e){
      e.preventDefault();
      Checkout.pagarmeCheckout();
      return false;
    });
  },

  extractData: function(data){
    $.each(data, function (index, value) {
      if (typeof value === 'object' && value != null) {
        Checkout.extractData(value);
      } else {
        Checkout.addFieldsToForm(Checkout.checkoutForm, index, value)
      }
    })
  },

  addFieldsToForm: function(form, field, value){
    form.append($("<input type='hidden' name='"+ field +"'>").val(value));
  },

  pagarmeCheckout: function() {
    var encryption_key = $("meta[name='pagarme-encryption-key']").attr("content")
    Checkout.setPaymentService("pagarme")
    Checkout.setFormAttributes()
    var pagarme = new PagarMeCheckout.Checkout({"encryption_key": encryption_key,
      success: function(data) {
        Checkout.extractData(data)
        Checkout.checkoutForm.get(0).submit()
      },
      error: function(err) {
      }
    });

    var params = {
      "customerData": "true",
      "createToken": "false",
      "uiColor": "#7368B3",
      "buttonText": 'Apoiar Campanha',
      "paymentButtonText": 'Apoiar Campanha',
      "customerEmail": Checkout.checkoutForm.data('customer-email'),
      "amount": Checkout.parsedAmount,
      "customerName": Checkout.checkoutForm.data('customer-name'),
      "customerDocumentNumber": Checkout.checkoutForm.data('customer-document-number'),
      "customerPhoneDdd": Checkout.checkoutForm.data('customer-phone-ddd'),
      "customerPhoneNumber": Checkout.checkoutForm.data('customer-phone-number'),
      "customerAddressNeighborhood": Checkout.checkoutForm.data('customer-address-neighborhood'),
      "customerAddressStreet": Checkout.checkoutForm.data('customer-address-street'),
      "customerAddressStreetNumber": Checkout.checkoutForm.data('customer-address-street-number'),
      "customerAddressComplementary": Checkout.checkoutForm.data('customer-address-complementary'),
      "customerAddressZipcode": Checkout.checkoutForm.data('customer-address-zipcode')
    };

    if (Checkout.verify()) {
      pagarme.open(params);
    }
  },

  paypalCheckout: function(data) {
    // Ajax para api para lidar com o approval do paypal
    Checkout.setPaymentService("paypal")
    Checkout.extractData(data)
    Checkout.checkoutForm.get(0).submit()
  },

  setFormAttributes: function() {
    Checkout.setAmount()
    Checkout.min_value = parseFloat(Checkout.checkoutForm.data('min-value'))
    Checkout.logged = Checkout.checkoutForm.data("logged")
  },

  setPaymentService: function(service) {
    Checkout.extractData({
      payment_service: service
    })
  },

  configurePaypalButton: function() {
    paypal.Buttons({
      style: {
        layout: "horizontal",
        tagline: false
      },
      createOrder: function(data, actions) {
        return actions.order.create({
          intent: "CAPTURE",
          purchase_units: [{
            amount: {
              currency_code: "BRL",
              value: Checkout.amount
            }
          }]
        })
      },
      onApprove: function(data) {
        Checkout.closeModal()
        Checkout.paypalCheckout(data)
      },
      onClose: function() {
        Checkout.closeModal()
      },
      onCancel: function() {
        Checkout.closeModal()
      }
    }).render("#paypal-button-container")
  },

  setAmount: function() {
    raw_amount = Checkout.checkoutForm.find('#checkout_amount')[0].value
    Checkout.amount = (raw_amount === "") ? 0 : parseFloat(raw_amount)
    Checkout.parsedAmount = parseInt(Checkout.amount, 10) * 100
  },

  verify: function() {
    if (!Checkout.logged) {
      toastr.warning('Você precisa estar logado para apoiar uma campanha!', 'Oooops!')
      window.location.replace("/users/sign_in")
      return false
    } else if (!Checkout.amount || Checkout.amount == 0) {
      toastr.error('Valor não pode ser em branco!', 'Ocorreu um erro')
      return false
    } else if (Checkout.amount < Checkout.min_value) {
      toastr.error('Valor não pode ser inferior ao mínimo', 'Ocorreu um erro')
      return false
    }
    return true
  },

  closeModal: function() {
    $("#payment-service-modal").modal('hide')
  }
}

$(document).ready(function() {
  Checkout.setup()
})
