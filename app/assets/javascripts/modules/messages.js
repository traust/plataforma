var Messages = {
  setup: function() {
    Messages.scroll()

    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
      Messages.scroll()
    })
  },

  scroll: function() {
    var messageContainer = $('#messages')
    if (messageContainer.length > 0) {
      messageContainer.animate({ scrollTop: $('#messages')[0].scrollHeight }, 'slow');
    }
  }
}

$(document).on("turbolinks:load", function() {
  Messages.setup();
})
