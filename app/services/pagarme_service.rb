class PagarmeService
  def initialize(user, project, params={})
    @user = user if user.present?
    @params = params
    @project = project.present? ? project : Project.find(@param[:project_id])
  end

  def process
    begin
      transaction = PagarMe::Transaction.new(transaction_config)
      transaction.charge
      return transaction
    rescue => error
      return false
    end
  end

  private

  def transaction_config
    email = @user.email
    default_split = ENV['DEFAULT_SPLIT_RATE'].to_i
    default_recipient_id = ENV['DEFAULT_RECIPIENT_ID']

    transaction_hash = {
      payment_method: "#{@params[:payment_method]}",
      card_hash: "#{@params[:card_hash]}",
      amount: "#{@params[:amount]}",
      postback_url: Rails.application.secrets.postback_url,
      customer: {
        name: "#{@params[:name]}",
        document_number: "#{@params[:document_number]}",
        email: email,
        address: {
          zipcode: "#{@params[:zipcode]}",
          street: "#{@params[:street]}",
          street_number: "#{@params[:street_number]}",
          complementary: "#{@params[:complementary]}",
          neighborhood: "#{@params[:neighborhood]}",
          city: "#{@params[:city]}",
          state: "#{@params[:state]}"
        },
        phone: {
          ddd: "#{@params[:ddd]}",
          number: "#{@params[:number]}"
        }
      },
      metadata: {
        project_id: "#{@params[:project_id]}",
        reward_id: "#{@params[:reward_id]}"
      }
    }

    if is_bank_account_configured?
      transaction_hash[:split_rules] = [{
        recipient_id: @project.bank_account.recipient_id,
        percentage: 100 - default_split
      },{
        recipient_id: default_recipient_id,
        percentage: alster_split,
        charge_processing_fee: true,
        liable: true
      }]
    end

    transaction_hash
  end

  def is_bank_account_configured?
    @project.bank_account.present? && @user.bank_accounts.present? && @project.bank_account.user_id == @user.id
  end
end

