require 'paypal-checkout-sdk'

class PaypalService
  def initialize(order_id)
    @order_id = order_id
    client_secret = Rails.application.secrets.paypal_client_secret
    client_id = Rails.application.secrets.paypal_client_id
    @environment = Rails.env.production? ?
      PayPal::LiveEnvironment.new(client_id, client_secret) :
      PayPal::SandboxEnvironment.new(client_id, client_secret)

    @client = PayPal::PayPalHttpClient.new(@environment)
  end

  def process
    begin
      capture_order.present? ? @capture_order_response.result : false
    rescue BraintreeHttp::HttpError => ioe
      return false
    end
  end

  private

  def capture_order
    @capture_order_response = @client.execute(PayPalCheckoutSdk::Orders::OrdersCaptureRequest::new(@order.result.id)) if get_order.present?
  end

  def get_order
    @order = @client.execute(PayPalCheckoutSdk::Orders::OrdersGetRequest::new(@order_id))
  end
end
