class BackerService
  def initialize(current_user, params={})
    @current_user = current_user if current_user.present?
    @params = params
  end

  def process
    @anonymous = @params[:checkout][:anonymous].present? ? @params[:checkout][:anonymous] : false
    @refund_if_not_success = @params[:checkout][:refund_if_not_success].present? ? @params[:checkout][:refund_if_not_success] : false
    @observation = @params[:checkout][:observation].present? ? @params[:checkout][:observation] : nil
    @project = Project.find(@params[:project_id])
    begin
      @backer = self.send("create_backer_#{@params[:payment_service]}")
      return @backer
    rescue Exception => e
      return false
    end
  end

  private

  def create_backer_pagarme
    @transaction = PagarmeService.new(@current_user, @project, @params).process
    address = {
      zipcode: @transaction.address.zipcode,
      address: @transaction.address.street,
      number: @transaction.address.street_number,
      complement: @transaction.address.complementary,
      neighborhood: @transaction.address.neighborhood,
      city: @transaction.address.city,
      state: @transaction.address.state
    }

    if @transaction.present? && address.present?
      backer = Backer.new(
        user_id: @current_user.id,
        project_id: @transaction.metadata.project_id,
        reward_id: @transaction.metadata.reward_id,
        value: (@transaction.amount/100),
        status: @transaction.status,
        payment_method: @transaction.payment_method,
        payment_id: @transaction.id,
        payment_url: @transaction.boleto_url,
        anonymous: @anonymous,
        refund_if_not_success: @refund_if_not_success,
        observation: @observation,
        neighborhood: address[:neighborhood],
        city: address[:city],
        street_number: address[:number],
        state: address[:state],
        complement: address[:complement],
        zip_code: address[:zipcode],
        street: address[:address]
      )

      backer.save! if backer.valid?
      if @current_user.address.present?
        @current_user.address.update(address)
      else
        @current_user.address = Address.new(address)
      end
      @current_user.phone = @transaction.phone.number
      @current_user.phone_area_code = @transaction.phone.ddd
      @current_user.document = @transaction.customer.document_number
      @current_user.save!
      return backer
    else
      return false
    end
  end

  def create_backer_paypal
    @order = PaypalService.new(@params[:orderID]).process
    if @order.present?
      backer = Backer.new(
        user_id: @current_user.id,
        project_id: @params[:project_id],
        reward_id: @params[:reward_id],
        value: @params[:checkout][:amount],
        status: get_status(@order),
        payment_method: @params[:payment_service],
        payment_id: @params[:orderID],
        anonymous: @anonymous,
        refund_if_not_success: @refund_if_not_success,
        observation: @observation
      )
      backer.save! if backer.valid?
      return backer
    else
      return false
    end
  end

  def get_status(order)
    status = order.status
    if status.present?
      case status
      when "COMPLETED"
        return :paid
      when "APPROVED"
        return :authorized
      when "SAVED", "CREATED"
        return :waiting_payment
      when "VOIDED"
        return :refused
      end
    end
  end
end
