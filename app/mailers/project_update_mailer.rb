class ProjectUpdateMailer < ApplicationMailer
  def new_update_email(project_update)
    project = project_update.project
    if project.backers.count > 0
      @project_update = project_update
      @project_name = project.name
      
      project.backers.each do |backer|
        @backer = backer
        @backer_user = backer.user

        mail(to: @backer_user.email, subject: 'Uma nova atualizaçāo sobre um projeto apoiado por você!')
      end
    end
  end
end