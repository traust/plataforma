class RegistrationMailer < ApplicationMailer
  def welcome_imported(user, password)
    @user = user
    @password = password

    mail(to: @user.email, subject: "Sua nova conta em alster.esp.br")
  end
end