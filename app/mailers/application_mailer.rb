class ApplicationMailer < ActionMailer::Base
  default from: "\"Alster\" <no-reply@alster.esp.br>"
  layout 'mailer'
end
