class BackerMailer < ApplicationMailer
  def receipt_mail(backer, amount)
    @user = backer.user
    @project = backer.project
    @payment_url = backer.payment_url
    @reward = backer.reward
    @amount = amount
    mail(to: @user.email, subject: 'Confirmação de doação!')
  end
end
