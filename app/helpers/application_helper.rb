module ApplicationHelper
  def amount_to_real(amount)
    number_to_currency(amount, unit: "R$ ", separator: ",", delimiter: "", precision: 2)
  end

  def amount_to_float(amount)
    number_with_precision(amount, precision: 2, separator: ",", delimiter: "")
  end

  def facebook_image(project = nil)
    project.nil? || project.image_url.blank? ? "logo.png" : project.try(:image_url)
  end
end
