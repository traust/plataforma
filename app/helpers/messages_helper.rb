module MessagesHelper
  def set_message_user(conversation)
    is_sender?(conversation.sender) ? conversation.recipient : conversation.sender
  end

  def set_user_name(user)
    is_sender?(user) ? "Você" : user.name
  end

  def set_message_class(message)
    is_sender?(message.user) ? "messages-sent" : "messages-received"
  end

  def is_sender?(user)
    user === current_user
  end

  def user_image(user)
    user.present? && user.avatar_url.present? ? user.avatar_url : 'https://cdn.filepicker.io/api/file/Deo6ULjmQNuRX6pPBUQi'
  end

  def user_name(user)
    user.present? && user.name.present? ? user.name : "Não Informado"
  end

  def project_name(project)
    project.present? && project.name.present? ? project.name : "Não Informado"
  end

  def unread_messages_count(conversation, user)
    conversation.messages.only_unread(user).count if conversation.present?
  end

  def has_unread_messages?(conversation, user)
    unread_messages_count(conversation, user) > 0 if conversation.present?
  end

  def unread_conversation_badge(conversation, user)
    counter = unread_messages_count(conversation, current_user) if conversation.present?
    render_unread_badge(counter)
  end

  def render_unread_badge(counter)
    has_unread = counter > 0
    badge_theme = has_unread ? 'danger' : 'secondary'
    text_theme = has_unread ? 'text-light' : 'text-secondary'
    html = "<span class='badge badge-pill badge-#{badge_theme}'>"
    html += "<span class='#{text_theme}'>#{counter}</span>"
    html += "</span>"
    html.html_safe
  end

  def format_message(message)
    simple_format(message.body).gsub(URI.regexp, '<a class="text-secondary" href="\0">\0</a>').html_safe
  end
end
