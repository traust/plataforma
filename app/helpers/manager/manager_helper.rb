module Manager::ManagerHelper
  def is_active_controller?(controller_name)
    params[:controller] == controller_name ? "active" : nil
  end

  def is_controller?(controller_name)
    params[:controller] == controller_name
  end

  def is_active_action?(action_name)
    params[:action] == action_name ? "active" : nil
  end

  def is_action?(action_name)
    params[:action] === action_name
  end

  def format_date(date)
    l date.to_date, :format => :default if date.present?
  end

  def format_hour(hour)
    l hour.to_time, :format => :default
  end

  def amount_to_real(amount)
    number_to_currency(amount, unit: "R$ ", separator: ",", delimiter: "")
  end

  def amount_to_price(amount)
    number_with_precision(amount, precision: 2, separator: ",", delimiter: "")
  end

  def percentage_to_number(value)
    number_with_precision(value, precision: 2, separator: ',')
  end

  def path_new_exists?(controller)
    controller_name = controller.gsub('/', '_')
    begin
      Rails.application.routes.recognize_path(send("new_#{(controller_name).singularize}_path"))
    rescue => exception
      return false
    end

    return true
  end

  def path_to_new(controller)
    controller_name = controller.gsub('/', '_')
    send("new_#{(controller_name).singularize}_path")
  end
end
