module PostHelper
  def selected_category
    (params[:q].present? && params[:q]["post_category_id_eq"].present?) ?
      params[:q]["post_category_id_eq"] : nil
  end
end
