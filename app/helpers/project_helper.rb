module ProjectHelper
  def bn_image(project)
    "background-image: linear-gradient(90deg, rgba(0,56,77, 80%), rgba(0,56,77,0) 50%), linear-gradient(180deg, rgba(0,56,77,80%), rgba(0,56,77,0) 50%), url('#{project.image_url}');"
  end

  def format_month_year_date(date, format = '%m/%Y')
    l date.to_date, :format => format
  end

  def show_time_left(project)
    difference = time_left(project)

    if (difference.nil? || difference < 0)
      "Encerrada"
    elsif (difference == 0)
      "Encerra hoje"
    else
      "#{difference} dias restantes"
    end
  end

  def time_left(project)
    project.expires_at.present? ? (project.expires_at - Date.today).to_i : nil
  end

  def reward_can_be_backed?(reward)
    backers_reward_count = Backer.only_not_canceled.where(reward: reward).count
    project_can_be_backed?(reward.project) && reward.maximum_backers.present? && backers_reward_count < reward.maximum_backers
  end

  def project_can_be_backed?(project)
    difference = time_left project
    !difference.nil? && difference >= 0
  end

  def status_label(backer)
    backer.status === "paid" ? "Aprovado" : "Aguardando Compensaçāo"
  end
end
