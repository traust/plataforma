class NotificationsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create

  def create
    backer = Backer.where(payment_id: params[:id]).take

    if backer.present?
      backer.update_attributes(status: params[:current_status])
    end

    head :ok
  end
end
