class ProjectsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create]
  before_action :project_categories, only: [:new, :edit]
  before_action :backers, only: [:show, :new, :edit]
  before_action :drafts, only: [:index]
  before_action :bank_account, only: [:create, :update]
  before_action :set_status, only: [:create, :update]
  before_action :message, only: [:show]
  after_action :read_messages, only: [:show]

  def index
    respond_with collection
  end

  def show
    respond_with object
  end

  def new
    respond_with object
  end

  def edit
    respond_with object
  end

  def create
    if object.valid?
      object.save
      flash[:notice] = 'Campanha criada com sucesso!'
      redirect_to @project
    else
      flash[:notice] = 'Erro ao salvar campanha. Tente novamente!'
      redirect_back fallback_location: root_path
    end
  end

  def update
    if object.valid?
      object.update(project_params)
      flash[:notice] = 'Campanha atualizada com sucesso!'
      redirect_to controller: params[:controller], action: :index
    else
      flash[:notice] = 'Erro ao salvar campanha. Tente novamente!'
      redirect_back fallback_location: root_path
    end
  end

  private

  def set_status
    return unless params[:status].present?

    case params[:status]
    when "send_to_approve"
      object.send_to_approve!
    when "publish"
      object.publish!
    end
  end

  def message
    @conversation = set_convesation
    @message = @conversation.messages.new
  end

  def stored_conversation
    Conversation.where(project: object, sender: current_user).take
  end

  def set_convesation
    stored_conversation.present? ?
      stored_conversation :
      object.conversations.create({ sender: current_user })
  end

  def read_messages
    object.messages.only_unread(current_user).update(is_read: true)
  end

  def default_recipient
    user_id = ENV["DEFAULT_USER_ID_TO_CONVERSATION"]
    User.find(user_id)
  end

  def project_categories
    @project_categories = ProjectCategory.all
  end

  def collection
    @q = Project.only_active.order_highlight.ransack(params[:q])
    @projects = @q.result(distinct: true).paginate(:page => params[:page], :per_page => 40)
  end

  def drafts
    @project_drafts = current_user.projects.only_drafts if current_user.present?
  end

  def object
    @project ||= params[:id].present? ? Project.friendly.find(params[:id]) : current_user.projects.new(project_params)
    @project.bank_account = @bank_account if @bank_account.present?
    return @project
  end

  def backers
    if object.present?
      @backers = (can? :manage, @project) ? @project.backers : @project.backers.only_paid
    end
  end

  def bank_account
    @bank_account = BankAccount.new(project_params[:bank_account]) if project_params[:bank_account].present?
    @bank_account.user = current_user
    @bank_account.save! if @bank_account.valid?
    params[:project].delete(:bank_account) if @bank_account.present?
  end

  def project_params
    params.fetch(:project, {}).permit(:name, :goal, :expires_at, :about, :headline, :video_url,
      :image_url, :budget_image_url, :status, :project_category_id, :slug, rewards_attributes: [:id, :delivery_date, :minimum_value,
      :maximum_backers, :observation_description, :description, :image_url, :position, :_destroy], bank_account: [:bank_code, :agency, :agency_vd,
        :account, :account_vd, :document_number, :legal_name])
  end
end
