class PostsController < ApplicationController
  before_action :post_categories
  before_action :authenticate_user!, only: [:create, :new]
  before_action :set_post, only: [:create, :new]
  before_action :object

  respond_to :html, :js

  def index
    respond_with collection
  end

  def show
    respond_with object
  end

  def create
    if @post.valid?
      @post.save
    end

    respond_with @post
  end

  def destroy
    object.destroy
    redirect_to posts_path
  end

  private

  def collection
    @q = Post.order_by_date.ransack(params[:q])
    @posts = @q.result(distinct: true).paginate(:page => params[:page], :per_page => 5)
  end

  def post_categories
    @post_categories = PostCategory.all
  end

  def set_post
    @post = Post.friendly.find(params[:id]) if params[:id].present?
  end

  def object
    @post = params[:id].present? ? Post.friendly.find(params[:id]) : Post.create(post_params)
  end

  def post_params
    params.fetch(:post, {}).permit(:title, :body, :image_url, :post_category_id)
  end

end
