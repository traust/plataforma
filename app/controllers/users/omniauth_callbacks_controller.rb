class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def google_oauth2
    if user.persisted?
      set_flash_message(:notice, :success, :kind => "Google") if is_navigational_format?
      sign_in_and_redirect @user, event: :authentication
    else
      session["devise.google_data"] = request.env["omniauth.auth"].except(:extra)
      redirect_to '/'
    end
  end

  def facebook
    if user.persisted?
      sign_in_and_redirect @user, event: :authentication
    else
      redirect_to '/'
    end
  end

  private

  def user
    @user = User.from_omniauth(request.env["omniauth.auth"])
  end
end
