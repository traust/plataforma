class BackersController < ApplicationController
  before_action :authenticate_user!

  load_and_authorize_resource

  def show
    respond_with backer
  end

  private

  def backer
    @backer = params[:id].present? ? current_user.backers.find(params[:id]) : Backer.new(backer_params)
  end

  def backers
    @backers = current_user.backers if current_user.present?
  end

  def backer_params
    params.fetch(:backer, {}).permit(:user_id, :project_id, :observation, :reward_id, :value, :status, :payment_method, :payment_id)
  end
end
