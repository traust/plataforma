class NewsletterController < ApplicationController
  respond_to :js

  def create
    create_mailchimp_member
  end

  private

  def create_mailchimp_member
    begin
      Gibbon::Request.lists(ENV["MAILCHIMP_LIST_ID"]).members.create(body: {email_address: newsletter_params[:email], status: "subscribed"})
    rescue Gibbon::MailChimpError => exception
      if exception.body["status"] == 400 && exception.body["title"] == "Invalid Resource"
        @error_message = t("newsletter_invalid_email")
      end
    end
  end

  def newsletter_params
    params[:newsletter]
  end
end
