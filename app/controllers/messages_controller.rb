class MessagesController < ApplicationController
  skip_before_action :verify_authenticity_token

  respond_to :js

  def new
   respond_with message
  end

  def create
    if message.valid?
      message.save and message.conversation.touch
      # MessageMailer.new_message(recipient, message).deliver_later if recipient.present?
    end
  end

  private

  def message
    @message ||= params[:id].present? ? Message.find(params[:id]) : Message.new(message_params)
  end

  def message_params
    params.fetch(:message, {}).permit(:body, :conversation_id).merge(user: current_user)
  end
end
