class HomeController < ApplicationController
  before_action :projects

  def show
  end

  private

  def projects
    @projects = Project.only_active.order_highlight.limit(20)
  end
end
