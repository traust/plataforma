class ProjectUpdatesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create]
  before_action :set_project, only: [:create, :update]
  before_action :object  #When not implemented, the object gets on the views with nil timestamps

  respond_to :js

  def create
    if @project_update.valid?
      @project_update.save
      ProjectUpdateMailer.new_update_email(@project_update).deliver_later
    end

    respond_with @project_update
  end
  
  def update
    @project_update.update(project_update_params)
    respond_with @project_update
  end
  
  private
  
  def object
    @project_update = params[:id].present? ? ProjectUpdate.find(params[:id]) : ProjectUpdate.new(project_update_params)
  end

  def set_project
    @project = Project.friendly.find(params[:project_id]) if params[:project_id].present?
  end

  def project_update_params
    params.fetch(:project_update, {}).permit(:title, :description).merge(user_id: current_user.id,
      project_id: @project.id)
  end
end
