class CheckoutsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create
  before_action :project, only: :create

  def create
    if @project.present? && @project.visible && @project.expires_at >= Date.new && params[:payment_service].present?
      backer = BackerService.new(current_user, params).process
      return redirect_to backer_path(backer) if backer.present?
      flash[:error] = "Ocorreu um erro ao tentar completar a transação. Tente novamente mais tarde"
      redirect_back fallback_location: request.referrer
    end
  end

  private

  def project
    @project = Project.find(params[:project_id])
  end
end
