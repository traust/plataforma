class Manager::BaseController < ApplicationController
  layout "manager"

  before_action :check_permission

  respond_to :html, :js

  add_breadcrumb "Dashboard", controller: "dashboard", action: :show

  def index
    add_breadcrumb "#{t("controller_name.#{params[:controller]}")}", action: :index

    respond_with(:manager, collection)
  end

  def show
    add_breadcrumb "#{t("controller_name.#{params[:controller]}")}", action: :index
    add_breadcrumb "Visualização", action: :show

    respond_with(:manager, object)
  end

  def new
    respond_with(:manager, object)
  end

  def edit
    add_breadcrumb "#{t("controller_name.#{params[:controller]}")}", action: :index
    add_breadcrumb "Editar #{t("controller_name.#{params[:controller]}").singularize}", action: :new

    respond_with(:manager, object)
  end

  def create
    if object.valid?
      object.save
      flash[:notice] = 'Criado com sucesso!'
      redirect_to controller: params[:controller], action: :index
    else
      flash[:notice] = 'Erro ao salvar. Tente novamente!'
      redirect_back(fallback_location: root_path)
    end
  end

  def update
    object.update(object_params)
    flash[:notice] = 'Atualizado com sucesso!'
    redirect_to controller: params[:controller], action: :index
  end

  def destroy
    object.destroy if can? :destroy, object
    respond_with(:manager, object)
  end

  def check_permission
    redirect_to root_path if cannot? :manage, :all
  end
end