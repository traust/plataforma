class Manager::ProjectsController < Manager::BaseController
  private

  def object
    @project ||= params[:id].present? ? Project.friendly.find(params[:id]) : Project.new(object_params)
  end

  def collection
    @projects ||= Project.all
  end

  def object_params
    params.fetch(:project, {}).permit(:id, :name, :user_id, :project_category_id, :goal, :expires_at, :about,
      :headline, :video_url, :image_url, :slug, :status, :visible,
      rewards_attributes: [:id, :minimum_value, :maximum_backers, :observation_description, :delivery_date, :description, :image_url, :_destroy])
  end
end
