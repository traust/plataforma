class Manager::RewardsController < Manager::BaseController
  private

  def object
    @reward ||= params[:id].present? ? Reward.find(params[:id]) : Reward.new(object_params)
  end

  def collection
    @rewards ||= Reward.all
  end

  def object_params
    params.fetch(:reward, {}).permit(:id, :minimum_value, :project, :maximum_backers, :image_url, :created_at, :updated_at)
  end
end
