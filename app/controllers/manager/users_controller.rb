class Manager::UsersController < Manager::BaseController
  private

  def object
    @user ||= params[:id].present? ? User.find(params[:id]) : User.new(object_params)
  end

  def collection
    @users ||= User.all
  end

  def object_params
    params.fetch(:user, {}).permit(:id, :name, :email, :password, :password_confirmation)
  end
end
