class Manager::ConversationsController < Manager::BaseController
  respond_to :html

  def show
    add_breadcrumb t("controller_name.manager/conversations"), manager_conversations_path
    add_breadcrumb object.sender.name, manager_conversations_path(object)
    object.messages.only_unread(current_user).update(is_read: true)

    respond_with object
  end

  private

  def object
    @conversation = params[:id].present? ? Conversation.find(params[:id]) : Conversation.new(object_params)
  end

  def collection
    @conversations = Conversation.all
  end

  def object_params
    params.fetch(:message, {}).permit(:recipient_id, :project_id).merge(sender_id: current_user.id)
  end
end
