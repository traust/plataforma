class Manager::BackersController < Manager::BaseController
  private

  def object
    @backer ||= params[:id].present? ? Backer.find(params[:id]) : Backer.new(object_params)
  end

  def collection
    @q ||= Backer.ransack(params[:q])
    @backers ||= @q.result(distinct: true)
  end

  def object_params
    params.fetch(:backer, {}).permit(:id, :user_id, :project_id, :value, :reward_id, :status, :created_at)
  end
end
