class ContactsController < ApplicationController
  def show
  end
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if @contact.deliver
      flash.now[:notice] = 'Obrigado por enviar sua mensagem. Entraremos em contato em breve!'
    else
      flash.now[:error] = 'Não foi possível enviar sua mensagem.'
    end

    redirect_to "/contact"
  end

end
