class Post < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :post_category
  
  scope :order_by_name, -> { order(title: :asc) }
  scope :order_by_date, -> { order(created_at: :desc) }

  def summary
    text = self.body.blank? ? "" : self.body
    text.truncate(115, separator: /\s/)
  end
end
