class Reward < ApplicationRecord
  belongs_to :project

  scope :order_by_position, -> { order("position ASC") }

  def minimum_valuee
    self.minimum_value.round(2)
  end
end
