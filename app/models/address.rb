class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true

  def location
    city.present? ? "#{city}, #{state}" : ''
  end

  def full_address
    "#{address}, #{number} / #{complement}, #{city}, #{state}"
  end
end
