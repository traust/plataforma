class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    if user.admin?
      can :manage, :all
      can :access, :rails_admin
      can :read, :dashboard
    else
      can :read, :all, user_id: user.id
      can :manage, Project, user_id: user.id
    end
  end
end
