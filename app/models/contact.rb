class Contact < MailForm::Base
  attribute :name
  attribute :email, validate: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject
  attribute :message


  def headers
    {
      :subject => "Fale Conosco - Alster",
      :to => "info@alster.esp.br",
      :from => %("#{name}" <#{email}>)
    }
  end

end
