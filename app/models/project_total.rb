class ProjectTotal < ActiveRecord::Base
  belongs_to :project

  def self.by(project)
    where(project: project)
  end

  def self.refresh
    Scenic.database.refresh_materialized_view(table_name, concurrently: false, cascade: false)
  end
end
