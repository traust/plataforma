class Backer < ApplicationRecord
  PAID_STATES = [:processing, :authorized, :paid, :refunded, :waiting_payment, :pending_refund, :refused]

  belongs_to :project
  belongs_to :reward, optional: true
  belongs_to :user

  scope :only_paid, -> { where(status: :paid) }
  scope :only_not_canceled, -> { where(status: [:paid, :authorized, :processing, :waiting_payment]) }
end
