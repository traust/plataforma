class Project < ApplicationRecord
  include AASM
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :user
  belongs_to :bank_account, optional: true
  belongs_to :project_category

  has_one :project_total
  has_many :backers
  has_many :updates, class_name: 'ProjectUpdate'
  has_many :rewards
  has_many :conversations
  has_many :messages, through: :conversations

  accepts_nested_attributes_for :rewards, reject_if: :all_blank, allow_destroy: true

  scope :with_project_totals, -> { joins('LEFT OUTER JOIN project_totals ON project_totals.project_id = projects.id') }
  scope :order_highlight, -> { where(highlight: true).order(id: :desc) }
  scope :only_active, -> { where.not(status: [:draft, :awaiting_approval, :rejected]).where(visible: true).order(id: :desc) }
  scope :only_drafts, -> { where(status: [:draft, :awaiting_approval, :rejected]) }

  default_scope { order("created_at DESC") }

  aasm column: :status do
    state :draft, initial: true
    state :awaiting_approval
    state :rejected
    state :approved
    state :in_progress
    state :successful
    state :failed

    event :send_to_approve do
      transitions from: [:draft, :rejected], to: :awaiting_approval
    end

    event :approve do
      transitions from: [:draft, :rejected, :awaiting_approval], to: :approved
    end

    event :reject do
      transitions from: [:draft, :approved, :awaiting_approval], to: :rejected
    end

    event :publish do
      transitions from: [:draft, :approved, :awaiting_approval], to: :in_progress
    end

    event :success do
      transitions from: :in_progress, to: :successful
    end

    event :fail do
      transitions from: :in_progress, to: :failed
    end
  end


  def summary
    text = self.headline.blank? ? self.about : self.headline
    text.truncate(115, separator: /\s/)
  end

  def total
    self.backers.only_paid.sum(:value)
  end

  def progress
    progress = goal.blank? || goal.zero? ? 100 : (total / goal * 100)
    progress.to_f.round(2)
  end
end
