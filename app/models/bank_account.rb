class BankAccount < ApplicationRecord
  before_create :set_recipient

  validates :bank_code, presence: true
  validates :agency, presence: true
  validates :account, presence: true
  validates :account_type, presence: true
  validates :legal_name, presence: true
  validates :document_number, presence: true

  belongs_to :user
  has_many :projects

  def set_recipient
    begin
      bank_account_hash = {
        bank_code: self.bank_code,
        agencia: self.agency,
        conta: self.account,
        type: self.account_type,
        legal_name: self.legal_name,
        document_number: self.document_number
      }

      bank_account_hash[:conta_dv] = self.account_vd if self.account_vd.present?
      bank_account_hash[:agencia_dv] = self.agency_vd if self.agency_vd.present?
      bank_account = PagarMe::BankAccount.create(bank_account_hash)
      recipient_id = PagarMe::Recipient.create({
        transfer_enabled: true,
        bank_account_id: bank_account.id
      }).id
    rescue Exception => e
      nil
    end
    self.recipient_id = recipient_id
  end
end
