class User < ApplicationRecord
  has_one :address, as: :addressable,
      inverse_of: :addressable, dependent: :destroy

  accepts_nested_attributes_for :address,
    allow_destroy: true,
    reject_if: :reject_address

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2, :facebook]

  has_many :backers
  has_many :projects
  has_many :bank_accounts
  has_many :conversations
  has_many :messages, through: :projects

  def self.from_omniauth(auth)

    data = auth.info
    user = User.where(email: data['email']).first

    unless user
      user = User.create(name: data['name'],
        email: data['email'],
        token: auth.credentials.token,
        expired: auth.credentials.expires,
        expires_at: auth.credentials.expires,
        refresh_token: auth.credentials.refresh_token,
        provider: auth.provider,
        avatar_url: data['image'],
        password: Devise.friendly_token[0,20])
    else
      if !user.token.present?
        user.token = auth.credentials.token
        user.expired = auth.credentials.expires
        user.expires_at = auth.credentials.expires_at
        user.refresh_token = auth.credentials.refresh_token
        user.provider = auth.provider
        user.save!
      end

      user.avatar_url = data['image'] unless user.avatar_url.present?
    end

    user
  end

  def image_url
    avatar_url.present? ? avatar_url : 'no-image.png'
  end
end
