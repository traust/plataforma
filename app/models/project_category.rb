class ProjectCategory < ApplicationRecord
  has_many :projects

  scope :ordered_by_name, -> { order(name: :asc) }
end
