class PostCategory < ApplicationRecord
  has_many :posts

  scope :order_by_name, -> { order(name: :asc) }
end
