module Addressable
  extend ActiveSupport::Concern

  included do
    has_one :address, as: :addressable,
      inverse_of: :addressable, dependent: :destroy

    accepts_nested_attributes_for :address,
      allow_destroy: true,
      reject_if: :reject_address

    private

    def reject_address(attrs)
      attrs[:city].blank?
    end
  end
end
