class Conversation < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  belongs_to :recipient, class_name: 'User', required: false
  belongs_to :project
  has_many :messages

  scope :from_user, -> (user) { where('sender_id = :user OR recipient_id = :user', user: user) }
  scope :only_unread, -> { where(is_read: false) }

  accepts_nested_attributes_for :messages, allow_destroy: true, reject_if: :reject_message

  scope :between, -> (sender, recipient) {
    where("(conversations.sender_id = :sender AND conversations.recipient_id = :recipient) OR (conversations.sender_id = :sender AND conversations.recipient_id = :recipient)", sender: sender, recipient: recipient)
  }

  default_scope { order('conversations.updated_at DESC') }

  def users
    sender_id = sender.id if sender.present?
    recipient_id = recipient.id if recipient.present?
    User.where(id: [sender_id, recipient_id])
  end

  private

  def reject_message(attrs)
    attrs[:body].blank?
  end
end
