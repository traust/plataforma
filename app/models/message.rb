class Message < ApplicationRecord
  belongs_to :conversation
  belongs_to :user

  scope :from_user, -> (user) { where('sender_id = :user OR recipient_id = :user', user: user) }
  scope :from_sender, -> (user) { where('sender_id = :user', user: user) }
  scope :to_recipient, -> (user) { where('recipient_id = :user', user: user) }
  scope :only_unread, -> (user) { where.not(user: user).where(is_read: false) }

  scope :between, -> (sender, recipient) {
    where("(conversations.sender_id = :sender AND conversations.recipient_id = :recipient) OR (conversations.sender_id = :sender AND conversations.recipient_id = :recipient)", sender: sender, recipient: recipient)
  }
end
