Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  root to: "home#show"

  resource :home, only: [:show]
  resource :about, only: [:show]
  resource :terms, only: [:show]
  resource :contact, only: [:show, :new, :create]
  resources :projects do
    resources :project_updates
  end
  resources :checkouts, only: :create
  resources :backers, only: :show
  resources :notifications, only: :create
  resources :newsletter, only: :create
  resources :posts
  resources :messages, only: [:new, :create]

  namespace :manager do
    root to: 'dashboard#show'

    resources :dashboard, only: :show
    resources :projects
    resources :users
    resources :rewards
    resources :backers, except: [:create, :new]
    resources :posts
    resources :conversations
  end

  get ':project', to: redirect('/projects/%{project}')
end
