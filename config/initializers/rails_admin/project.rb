RailsAdmin.config do |config|
  config.model 'Project' do
    edit do
      include_all_fields
      field :status, :enum do
        enum ["draft","awaiting_approval","rejected","approved","in_progress","successful","failed"]
      end
    end
  end
end
