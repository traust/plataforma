RailsAdmin.config do |config|
  config.model 'User' do
    edit do
      field :email
      field :password
      field :name
      field :bio
      field :address
      field :phone
      field :document
      field :admin, :boolean
      field :avatar_url
    end
  end
end
